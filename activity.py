class Camper():
	# constructor
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	# methods
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program.")

	def info(self):
		print(f"My name is{self.name} of batch{self.batch}.")


# instantiate
zuitt_camper = Camper("Evo", "B-276", "Python Short Course")

print(zuitt_camper.name)
print(zuitt_camper.batch)
print(zuitt_camper.course_type)
print(zuitt_camper)

# Output
	# Evo
	# B-276
	# Python Short Course
	# <__main__.Camper object at 0x0000023993129880>

zuitt_camper.career_track()
zuitt_camper.info()

